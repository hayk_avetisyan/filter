package com.egs.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginFilter implements Filter{
    private final String login="admin";
    private final String password="password";

    public void init(FilterConfig arg0) throws ServletException {}

    public void doFilter(ServletRequest req, ServletResponse resp,
                         FilterChain chain) throws IOException, ServletException {

        PrintWriter out=resp.getWriter();

        String pass=req.getParameter("password");
        String log=req.getParameter("login");
        if(password.equals(pass) && login.equals(log)){
            chain.doFilter(req, resp);//sends request to next resource
        }
        else{
            out.print("username or password error!");
            RequestDispatcher rd=req.getRequestDispatcher("index.html");
            rd.include(req, resp);
        }

    }
    public void destroy() {}

}
